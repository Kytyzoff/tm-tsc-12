package ru.tsc.borisyuk.tm.service;

import ru.tsc.borisyuk.tm.api.repository.ITaskRepository;
import ru.tsc.borisyuk.tm.api.service.ITaskService;
import ru.tsc.borisyuk.tm.enumerated.Status;
import ru.tsc.borisyuk.tm.model.Task;

import java.util.List;

public class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public void create(final String name) {
        if (name == null || name.isEmpty()) return;
        final Task task = new Task();
        task.setName(name);
        taskRepository.add(task);
    }

    @Override
    public void create(final String name, final String description) {
        if (name == null || name.isEmpty()) return;
        if (description == null || description.isEmpty()) return;
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        taskRepository.add(task);
    }

    @Override
    public void add(final Task task) {
        if (task == null) return;
        taskRepository.add(task);
    }

    @Override
    public void remove(final Task task) {
        if (task == null) return;
        taskRepository.remove(task);
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public Task findById(final String id) {
        if (id == null || id.isEmpty()) return null;
        return taskRepository.findById(id);
    }

    @Override
    public Task findByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        return taskRepository.findByName(name);
    }

    @Override
    public Task findByIndex(final Integer index) {
        if (index == null || index < 0) return null;
        return taskRepository.findByIndex(index);
    }

    @Override
    public Task removeById(final String id) {
        if (id == null || id.isEmpty()) return null;
        return taskRepository.removeById(id);
    }

    @Override
    public Task removeByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        return taskRepository.removeByName(name);
    }

    @Override
    public Task removeByIndex(final Integer index) {
        if (index == null || index < 0) return null;
        return taskRepository.removeByIndex(index);
    }

    @Override
    public Task updateById(final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) return null;
        if (name == null || name.isEmpty()) return null;
        final Task task = findById(id);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateByIndex(final Integer index, final String name, final String description) {
        if (index == null || index < 0) return null;
        if (name == null || name.isEmpty()) return null;
        final Task task = findByIndex(index);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public boolean existsById(final String id) {
        if (id == null || id.isEmpty()) return false;
        return taskRepository.existsById(id);
    }

    @Override
    public boolean existsByIndex(final Integer index) {
        return taskRepository.existsByIndex(index);
    }

    @Override
    public Task startById(final String id) {
        if (id == null || id.isEmpty()) return null;
        return taskRepository.startById(id);
    }

    @Override
    public Task startByIndex(final Integer index) {
        if (index == null || index < 0) return null;
        return taskRepository.startByIndex(index);
    }

    @Override
    public Task startByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        return taskRepository.startByName(name);
    }

    @Override
    public Task finishById(final String id) {
        if (id == null || id.isEmpty()) return null;
        return taskRepository.finishById(id);
    }

    @Override
    public Task finishByIndex(final Integer index) {
        if (index == null || index < 0) return null;
        return taskRepository.finishByIndex(index);
    }

    @Override
    public Task finishByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        return taskRepository.finishByName(name);
    }

    @Override
    public Task changeStatusById(final String id, final Status status) {
        if (id == null || id.isEmpty()) return null;
        if (status == null) return null;
        return taskRepository.changeStatusById(id, status);
    }

    @Override
    public Task changeStatusByIndex(final Integer index, final Status status) {
        if (index == null || index < 0) return null;
        if (status == null) return null;
        return taskRepository.changeStatusByIndex(index, status);
    }

    @Override
    public Task changeStatusByName(final String name, final Status status) {
        if (name == null || name.isEmpty()) return null;
        if (status == null) return null;
        return taskRepository.changeStatusByName(name, status);
    }

}
