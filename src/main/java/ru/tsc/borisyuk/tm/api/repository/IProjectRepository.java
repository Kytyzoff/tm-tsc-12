package ru.tsc.borisyuk.tm.api.repository;

import ru.tsc.borisyuk.tm.enumerated.Status;
import ru.tsc.borisyuk.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    void add(Project project);

    void remove(Project project);

    void clear();

    List<Project> findAll();

    Project findById(String id);

    Project findByName(String name);

    Project findByIndex(Integer index);

    Project removeById(String id);

    Project removeByName(String name);

    Project removeByIndex(Integer index);

    boolean existsById(String id);

    boolean existsByIndex(Integer index);

    Project startById(String id);

    Project startByIndex(Integer index);

    Project startByName(String name);

    Project finishById(String id);

    Project finishByIndex(Integer index);

    Project finishByName(String name);

    Project changeStatusById(String id, Status status);

    Project changeStatusByIndex(Integer index, Status status);

    Project changeStatusByName(String name, Status status);

}
